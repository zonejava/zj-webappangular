#Stage1
#FROM node:latest as node
#MAINTAINER nanoo (arnaudlaval33@gmail.com)
#WORKDIR /app
#COPY . .
#RUN npm install
#RUN npm run build --prod

#Stage 2
#FROM nginx:alpine
#VOLUME /var/cache/nginx
#RUN rm -rf /usr/share/nginx/html/* && rm -rf /etc/nginx/nginx.conf
#COPY ./docker/nginx.conf /etc/nginx/nginx.conf
#COPY --from=node /app/dist /usr/share/nginx/html
#CMD ["nginx", "-g", "daemon off;"]

#Stage
FROM nginx:1.17.1-alpine
COPY /dist/ZJ-webapp-angular /usr/share/nginx/html

# When the container starts, replace the env.js with values from environment variables
#CMD ["/bin/sh",  "-c",  "envsubst < /usr/share/nginx/html/assets/env.template.js > /usr/share/nginx/html/assets/env.js"]
CMD ["/bin/sh",  "-c",  "envsubst < /usr/share/nginx/html/assets/env.template.js > /usr/share/nginx/html/assets/env.js && exec nginx -g 'daemon off;'"]
