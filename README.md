# Zone Java project - Angular web application
## Description
This is the web application of Zone Java project. 
Follow [this link](https://gitlab.com/zonejava/conception/-/blob/master/README.md) to see the project overview.
## Development
It has been developed with Webstorm IDEA 2020.1 and Angular 9.1.4
## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
## Owner
Arnaud Laval - arnaudlaval33@gmail.com

The Zone Java project is developed as part of final project of DA Java cursus (OpenClassrooms)
