(function(window) {
  window["env"] = window["env"] || {};

  // Environment variables
  window["env"]["gatewayUrl"] = "${GATEWAY_URL}";
})(this);
