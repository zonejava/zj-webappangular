import {Pipe, PipeTransform} from '@angular/core';
import {ChapterModel} from '../model/Chapter.model';

@Pipe({name: 'sortingByDate'})
export class SortingByDatePipe implements PipeTransform {

  transform(value: ChapterModel[], ...args): ChapterModel[] {
    return value.sort(
      (a,b) =>
        (a.creationChapterDate > b.creationChapterDate) ?
          1 :
          ((b.creationChapterDate > a.creationChapterDate) ? -1 : 0));
  }

}
