import {Component, ElementRef, OnInit, Renderer2, ViewChild} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TutorialModel} from '../../model/Tutorial.model';
import {Router} from '@angular/router';
import {TutorialService} from '../../service/tutorial.service';

@Component({
  selector: 'app-tutorial-form',
  templateUrl: './tutorial-form.component.html',
  styleUrls: ['./tutorial-form.component.css']
})
export class TutorialFormComponent implements OnInit {

  tutorialForm: FormGroup;
  errorMessage: string;
  result: TutorialModel;

  constructor(private formBuilder: FormBuilder,
              private tutorialService: TutorialService,
              private router: Router,
              private renderer: Renderer2) { }

  ngOnInit(): void {
    this.initForm();
    this.renderer.selectRootElement('#title').focus();
  }

  private initForm() {
    this.tutorialForm = this.formBuilder.group({
      title: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  onSaveTutorial() {
    const title = this.tutorialForm.get('title').value;
    const description = this.tutorialForm.get('description').value;
    const newTutorial = new TutorialModel(title, description);
    this.tutorialService.persistTutorialData(newTutorial).subscribe(
      response => {
        this.result = response;
        this.router.navigate(['/tutorial/redaction/' + this.result.externalId]); // TODO add some 'waiting' method while tutorial is create
      },
      (error) => {
        console.log('erreur : ' + error.message);
        this.errorMessage = error.message;
      }
    );
  }
}
