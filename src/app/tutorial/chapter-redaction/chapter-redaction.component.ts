import {Component, ElementRef, Input} from '@angular/core';
import {ChapterModel} from '../../model/Chapter.model';
import {ChapterService} from '../../service/chapter.service';
import {TutorialService} from '../../service/tutorial.service';
import {NgForm} from '@angular/forms';
import {CkeditorService} from '../../service/ckeditor.service';

@Component({
  selector: 'app-chapter-redaction',
  templateUrl: './chapter-redaction.component.html',
  styleUrls: ['./chapter-redaction.component.css']
})
export class ChapterRedactionComponent {

  @Input() chapter: ChapterModel;
  @Input() indexOfChapter: number;
  @Input() tutorialExternalId: string;
  @Input() editorOpen: boolean = false;

  videopannelOpen: boolean = false;
  ckeditorConfig = this.ckeditorService.getConfigOfCKEditor();

  constructor(private chapterService: ChapterService,
              private tutorialService: TutorialService,
              private ckeditorService: CkeditorService,
              private elementRef: ElementRef) { }

  onSupp(chapterExternalId: string) {
    this.tutorialService.removeChapterFromTutorial(chapterExternalId);
  }

  onSubmit(g: NgForm) {
    this.chapter.title = g.value['title'];
    this.chapter.content = g.value['content'];
    this.chapterService.saveChapter(this.chapter, this.tutorialExternalId);
  }

  onToggleEditorPannel(){
    const tag = this.elementRef.nativeElement.querySelector(".editor-panel");
    if (!tag.classList.contains('collapse')){
      tag.classList.add('collapse');
      this.editorOpen = false;
    }else {
      tag.classList.remove('collapse');
      this.editorOpen = true;
    }
  }

  onToggleVideoPannel() {
    const tag = this.elementRef.nativeElement.querySelector(".video-panel");
    if (!tag.classList.contains('collapse')){
      tag.classList.add('collapse');
      this.videopannelOpen = false;
    }else {
      tag.classList.remove('collapse');
      this.videopannelOpen = true;
    }
  }
}
