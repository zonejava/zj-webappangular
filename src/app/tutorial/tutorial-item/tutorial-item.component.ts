import {Component, Input, OnInit} from '@angular/core';
import {TutorialModel} from '../../model/Tutorial.model';
import {Router} from '@angular/router';

@Component({
  selector: 'app-tutorial-item',
  templateUrl: './tutorial-item.component.html',
  styleUrls: ['./tutorial-item.component.css']
})
export class TutorialItemComponent implements OnInit {

  @Input() tutorial: TutorialModel;
  @Input() indexOfTutorial: number;

  @Input() idCountMap: Map<string, number>;
  @Input() commentaryCount: number;
  videoCount: number;

  constructor(private router: Router) { }

  ngOnInit(): void {
    let i = 0;
    for (let chapter of this.tutorial.chapters) {
      if (chapter.video != null)
        i += 1;
    }
    this.videoCount = i;
  }

  onTutorial(externalId: string) {
    this.router.navigate(['tutorial/view/' + externalId]);
  }

}
