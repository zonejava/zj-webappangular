import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {TutorialModel} from '../../model/Tutorial.model';
import {Subscription} from 'rxjs';
import {TutorialService} from '../../service/tutorial.service';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CommentaryModel} from '../../model/Commentary.model';
import {CommentaryService} from '../../service/commentary.service';
import {TokenService} from '../../service/token.service';

@Component({
  selector: 'app-tutorial-view',
  templateUrl: './tutorial-view.component.html',
  styleUrls: ['./tutorial-view.component.css']
})
export class TutorialViewComponent implements OnInit {

  tutorial: TutorialModel;
  tutorialSubscription: Subscription;

  commentaryForm: FormGroup;
  commentaries: CommentaryModel[];
  commentariesSubscription: Subscription;

  loggedUserId: string;
  isLoggedUserAdmin: boolean;

  initialCommentaryValue: CommentaryModel;

  @ViewChild("commentary") contentField: ElementRef;

  constructor(private tutorialService: TutorialService,
              private commentaryService: CommentaryService,
              private tokenService: TokenService,
              private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.tutorialService.getTutorial(id);
    this.tutorialService.emitTutorial(); // TODO maybe remove
    this.tutorialSubscription = this.tutorialService.tutorialSubject.subscribe(
      (tutorial: TutorialModel) => {
        this.tutorial = tutorial;
      }
    );
    this.commentaryService.getCommentaries(id);
    this.commentaryService.emitCommentaries(); // TODO maybe remove
    this.commentariesSubscription = this.commentaryService.commentariesSubject.subscribe(
      (commentaries: CommentaryModel[]) => {
        this.commentaries = commentaries;
      }
    );
    this.initForm();
    this.loggedUserId = this.tokenService.payload.sub;
    this.isLoggedUserAdmin = this.tokenService.isAdmin();
  }

  initForm() {
    this.commentaryForm = this.fb.group({
      content: [this.initialCommentaryValue ? this.initialCommentaryValue.content : '', Validators.required]
    });
  }

  onSaveCommentary(tutorialId: string) {
    const content = this.commentaryForm.get('content').value;
    if (!this.initialCommentaryValue){
      const newCommentary = new CommentaryModel(tutorialId,content);
      this.commentaryService.saveCommentary(newCommentary);
    }else {
      this.initialCommentaryValue.content = content;
      this.commentaryService.updateCommentary(this.initialCommentaryValue);
    }
    this.commentaryForm.reset();
    this.initialCommentaryValue = null;
  }

  autogrow() {
    let textArea = document.getElementById("commentary");
    textArea.style.overflow = 'hidden';
    textArea.style.height = 'auto';
    textArea.style.height = textArea.scrollHeight + 'px';
  }

  updateEventHandler(commentary: CommentaryModel) {
    this.initialCommentaryValue = commentary;
    this.initForm();
    this.contentField.nativeElement.focus();
  }

  deleteEventHandler(commentaryId: string) {
    this.commentaryService.deleteCommentary(commentaryId,this.tutorial.externalId);
  }

}
