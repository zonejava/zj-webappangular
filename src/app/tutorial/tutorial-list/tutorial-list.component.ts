import {Component, OnDestroy, OnInit} from '@angular/core';
import {TutorialModel} from '../../model/Tutorial.model';
import {Subscription} from 'rxjs';
import {TutorialService} from '../../service/tutorial.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-tutorial-list',
  templateUrl: './tutorial-list.component.html',
  styleUrls: ['./tutorial-list.component.css']
})
export class TutorialListComponent implements OnInit, OnDestroy {

  keyword: string;

  tutorials : TutorialModel[];
  tutorialsSubscription: Subscription;
  currentPage: number = 1;
  currentPageSubscription: Subscription;
  pages: number[];
  pageSubscription: Subscription;
  idCountMap: Map<string, number>;
  idCountMapSubscription: Subscription;

  noResult:boolean = false;
  noResultSubscription: Subscription;
  errorConnection:boolean = false;
  errorConnectionSubscription: Subscription;

  navigationSubscription: Subscription;

  constructor(private tutorialService: TutorialService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.navigationSubscription = this.router.events.subscribe((e: any) => {
      // If it is a NavigationEnd event re-initalise the component
      if (e instanceof NavigationEnd) {
        this.initialiseInvites();
      }
    });
    this.keyword = this.route.snapshot.paramMap.get('search');
    this.getPage(0, this.keyword);
  }

  initialiseInvites() {
    this.keyword = this.route.snapshot.paramMap.get('search');
    this.getPage(0, this.keyword);
  }

  ngOnDestroy(){
    this.tutorialsSubscription.unsubscribe();
    this.pageSubscription.unsubscribe();
    this.currentPageSubscription.unsubscribe();
    this.idCountMapSubscription.unsubscribe();
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  getPage(page: number, keyword: string){
    this.tutorialService.getPublishedTutorials(page,keyword);
    this.tutorialsSubscription = this.tutorialService.tutorialsSubject.subscribe(
      (tutorials: TutorialModel[]) => {
        this.tutorials = tutorials;
      }
    );
    this.pageSubscription = this.tutorialService.pageSubject.subscribe(
      (pages: number[]) => {
        this.pages = pages;
      }
    );
    this.currentPageSubscription = this.tutorialService.currentPageSubject.subscribe(
      (number: number) => {
        this.currentPage = number;
      }
    );
    this.idCountMapSubscription = this.tutorialService.idCountMapSubject.subscribe(
      (count: Map<string,number>) => {
        this.idCountMap = count;
      }
    );
    this.noResultSubscription = this.tutorialService.noResultSubject.subscribe(
      (boolean: boolean) => {
        this.noResult = boolean;
      }
    );
    this.errorConnectionSubscription = this.tutorialService.errorConnectionSubject.subscribe(
      (boolean: boolean) => {
        this.errorConnection = boolean;
      }
    );
  }

  getFirstPage(){
    this.getPage(0, this.keyword);
  }

  getLastPage(){
    this.getPage(this.pages.length - 1, this.keyword);
  }

  getPreviousPage(){
    this.getPage(this.currentPage - 1, this.keyword);
  }

  getNextPage(){
    this.getPage(this.currentPage + 1, this.keyword);
  }

}
