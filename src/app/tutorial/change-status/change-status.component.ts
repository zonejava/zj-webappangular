import {Component, Input, OnInit} from '@angular/core';
import {TutorialModel} from '../../model/Tutorial.model';
import {TutorialService} from '../../service/tutorial.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-change-status',
  templateUrl: './change-status.component.html',
  styleUrls: ['./change-status.component.css']
})
export class ChangeStatusComponent implements OnInit {

  @Input() tutorial: TutorialModel;
  @Input() tutorialExternalId: string;
  @Input() statusModal;

  constructor(private tutorialService: TutorialService,
              private router: Router) { }

  ngOnInit(): void {
  }

  onPublish(){
    this.tutorialService.publish(this.tutorialExternalId);
    this.statusModal.hide();
    this.router.navigate(['/account/dashboard']);
  }

  onUnPublish(){
    this.tutorialService.unPublish(this.tutorialExternalId);
    this.statusModal.hide();
    this.router.navigate(['/account/dashboard']);
  }

}
