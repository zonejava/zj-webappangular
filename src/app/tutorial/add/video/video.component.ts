import {Component, Input, OnInit} from '@angular/core';
import {VideoModel} from '../../../model/Video.model';
import {ChapterService} from '../../../service/chapter.service';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {

  @Input() video: VideoModel;
  @Input() chapterExternalId: string;
  @Input() tutorialExternalId: string;
  @Input() videoModal;

  hasVideo: boolean = true;
  sources: string[] = ['YOUTUBE'];

  constructor(private chapterService: ChapterService) { }

  ngOnInit(): void {
    if (this.video == null)
      this.hasVideo = false;
    if (!this.hasVideo)
      this.video = new VideoModel('YOUTUBE','');
  }

  onSubmit(h: NgForm) {
    this.video.source = h.value['source'];
    this.video.link = h.value['link'];
    this.chapterService.addVideo(this.video, this.chapterExternalId, this.tutorialExternalId);
    this.hasVideo = true;
    this.videoModal.hide();
  }

  onDeleteVideo(videoExternalId:string){
    this.chapterService.deleteVideo(videoExternalId, this.tutorialExternalId);
    this.hasVideo = false;
    this.video = new VideoModel('YOUTUBE','');
    this.videoModal.hide();
  }

}
