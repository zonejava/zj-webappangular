import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {RepositoryModel} from '../../../model/Repository.model';
import {NgForm} from '@angular/forms';
import {TutorialService} from '../../../service/tutorial.service';

@Component({
  selector: 'app-repository',
  templateUrl: './repository.component.html',
  styleUrls: ['./repository.component.css']
})
export class RepositoryComponent implements OnInit {

  @Input() repository: RepositoryModel;
  @Input() tutorialExternalId: string;
  @Input() repositoryModal;

  @Output() deleteRepositoryEvent: EventEmitter<string> = new EventEmitter();
  @Output() updateRepositoryEvent: EventEmitter<RepositoryModel> = new EventEmitter();

  hasRepo: boolean = true;
  sources: string[] = ['GITHUB','GITLAB'];

  constructor(private tutorialService: TutorialService) { }

  ngOnInit(): void {
    if (this.repository == null)
      this.hasRepo = false;
    if (!this.hasRepo)
      this.repository = new RepositoryModel('GITHUB', '');
  }

  onSubmit(i: NgForm) {
    this.repository.source = i.value['source'];
    this.repository.link = i.value['link'];
    this.updateRepositoryEvent.emit(this.repository);
    this.hasRepo = true;
    this.repositoryModal.hide();
  }

  onDeleteRepository(repositoryExternalId:string){
    // this.tutorialService.deleteRepository(repositoryExternalId, this.tutorialExternalId);
    this.hasRepo = false;
    this.deleteRepositoryEvent.emit(repositoryExternalId)
    this.repository = new RepositoryModel('GITHUB', '');
    this.repositoryModal.hide();
  }

}
