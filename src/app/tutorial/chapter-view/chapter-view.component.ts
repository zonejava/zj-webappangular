import {Component, ElementRef, Input, OnInit} from '@angular/core';
import {ChapterModel} from '../../model/Chapter.model';
import {DomSanitizer, SafeResourceUrl} from '@angular/platform-browser';

@Component({
  selector: 'app-chapter-view',
  templateUrl: './chapter-view.component.html',
  styleUrls: ['./chapter-view.component.css']
})
export class ChapterViewComponent implements OnInit {

  @Input() chapter: ChapterModel;
  @Input() indexOfChapter: number;
  editorOpen: boolean = false;
  safeUrl: SafeResourceUrl;

  constructor(private elementRef: ElementRef,
              private _sanitizer: DomSanitizer) { }

  ngOnInit(): void {
    if (this.chapter.video)
      this.safeUrl = this._sanitizer.bypassSecurityTrustResourceUrl(this.chapter.video.link);
  }

  onToggle() {
    const tag = this.elementRef.nativeElement.querySelector('.content');
    if (!tag.classList.contains('collapse')) {
      tag.classList.add('collapse');
      this.editorOpen = false;
    } else {
      tag.classList.remove('collapse');
      this.editorOpen = true;
    }
  }

}
