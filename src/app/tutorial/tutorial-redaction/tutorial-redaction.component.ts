import {Component, OnDestroy, OnInit} from '@angular/core';
import {TutorialModel} from '../../model/Tutorial.model';
import {Subscription} from 'rxjs';
import {TutorialService} from '../../service/tutorial.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {ChapterModel} from '../../model/Chapter.model';
import {AccountService} from '../../service/account.service';
import {RepositoryModel} from '../../model/Repository.model';

@Component({
  selector: 'app-tutorial-redaction',
  templateUrl: './tutorial-redaction.component.html',
  styleUrls: ['./tutorial-redaction.component.css']
})
export class TutorialRedactionComponent implements OnInit, OnDestroy {

  tutorial: TutorialModel;
  tutorialSubscription: Subscription;

  constructor(private tutorialService: TutorialService,
              private accountService: AccountService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.tutorialService.getTutorial(id);
    this.tutorialService.emitTutorial();
    this.tutorialSubscription = this.tutorialService.tutorialSubject.subscribe(
      (tutorial: TutorialModel) => {
        this.tutorial = tutorial;
      }
    );
  }

  ngOnDestroy(): void {
    this.tutorialSubscription.unsubscribe();
  }

  onSubmit(f: NgForm) {
    this.tutorial.title = f.value['title'];
    this.tutorial.description = f.value['description'];
    this.tutorialService.saveTutorial(this.tutorial);
  }

  onDelete(tutorialId: string){
    this.tutorialService.deleteTutorial(tutorialId);
    this.accountService.getLoggedUserAccount();
    this.router.navigate(['/account/dashboard']);
  }

  onQuit(f: NgForm){
    this.onSubmit(f);
    this.router.navigate(['/account/dashboard']);
  }

  onAddChapter(tutorialId: string) {
    const chapterToSave = new ChapterModel();
    this.tutorialService.addChapterToTutorial(chapterToSave,tutorialId);
  }

  onPreview(){
    this.router.navigate(['/tutorial/view/' + this.tutorial.externalId]);
  }

  onUpdateRepositoryEventHandler(repository: RepositoryModel) {
    this.tutorialService.addRepository(repository, this.tutorial.externalId);
  }

  onDeleteRepositoryEventHandler(repositoryId: string) {
    this.tutorialService.deleteRepository(repositoryId, this.tutorial.externalId);
  }
}
