import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {WelcomeComponent} from './welcome/welcome.component';
import {AccountFormComponent} from './account/account-form/account-form.component';
import {UserDashboardComponent} from './account/user-dashboard/user-dashboard.component';
import {CustomGuard} from './guard/custom.guard';
import {TutorialFormComponent} from './tutorial/tutorial-form/tutorial-form.component';
import {TutorialRedactionComponent} from './tutorial/tutorial-redaction/tutorial-redaction.component';
import {TutorialViewComponent} from './tutorial/tutorial-view/tutorial-view.component';
import {TutorialListComponent} from './tutorial/tutorial-list/tutorial-list.component';


const appRoutes: Routes = [
  { path: 'welcome', component: WelcomeComponent},
  { path: 'tutorial/list', component: TutorialListComponent, runGuardsAndResolvers: 'always'},
  { path: 'account/dashboard', canActivate: [CustomGuard], component: UserDashboardComponent},
  { path: 'tutorial/create', canActivate: [CustomGuard], component: TutorialFormComponent},
  { path: 'tutorial/redaction/:id', canActivate: [CustomGuard], component: TutorialRedactionComponent},
  { path: 'tutorial/view/:id', component: TutorialViewComponent},
  { path: 'auth/signUp', component: AccountFormComponent},
  { path: 'auth/signIn', component: AccountFormComponent},
  { path: '', redirectTo: 'welcome', pathMatch: 'full'},
  { path: '**', redirectTo: 'welcome', pathMatch: 'full'}
]
@NgModule({
  imports: [RouterModule.forRoot(appRoutes, { useHash: true, onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
