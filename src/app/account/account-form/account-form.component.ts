import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder, FormControl,
  FormGroup,
  Validators
} from "@angular/forms";
import {Router} from "@angular/router";
import {AccountService} from "../../service/account.service";
import {UserAccountModel} from "../../model/UserAccount.model";
import {debounceTime, tap} from 'rxjs/operators';

@Component({
  selector: 'app-account-form',
  templateUrl: './account-form.component.html',
  styleUrls: ['./account-form.component.css']
})
export class AccountFormComponent implements OnInit {

  accountForm: FormGroup;
  errorMessage: string;
  result: UserAccountModel;
  loading: boolean;

  constructor(private formBuilder: FormBuilder,
              private accountService: AccountService,
              private router: Router) { }

  get email(){
    return this.accountForm.get('email') as FormControl;
  }

  get username(){
    return this.accountForm.get('username') as FormControl;
  }

  get password(){
    return this.accountForm.get('password') as FormControl;
  }

  get confirmation(){
    return this.accountForm.get('confirmation') as FormControl;
  }

  ngOnInit(): void {
    this.initForm();
    this.loading = false;
    this.checkMailAvailability();
    this.checkUsernameAvailability();
  }

  private initForm() {
    this.accountForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.maxLength(20)]],
      lastName: ['', [Validators.required, Validators.maxLength(20)]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/[0-9a-zA-Z]{8}/)]], // TODO find better regex
      confirmation: ['', Validators.required],
      username: ['', [Validators.maxLength(23), Validators.required]]
    }, {validator: this.checkPasswordConfirmation});
  }

  onSaveAccount() {
    this.errorMessage = null;
    this.loading = true;
    const firstName = this.accountForm.get('firstName').value;
    const lastName = this.accountForm.get('lastName').value;
    const email = this.accountForm.get('email').value;
    const password = this.accountForm.get('password').value;
    const username = this.accountForm.get('username').value;
    const newUserAccount = new UserAccountModel(firstName,lastName,username,email,password);
    this.accountService.createNewUser(newUserAccount).subscribe(
      response => {
        this.result = response;
        this.loading = false;
        this.router.navigate(['/welcome'], { state: { success: 'true' } });
      },
      (error) => {
        console.log(error.status.toString());
        console.log(error.error.message);
        this.loading = false;
        if (error.status == 0)
          this.errorMessage = 'Le serveur d\'authentification est momentanément indisponible, veuillez réessayer plus tard.';
        else
          this.errorMessage = error.error.message;
      }
    );
  }

  checkMailAvailability(){
    this.email.valueChanges.pipe(
      debounceTime(1000),
      tap((email) => {
        if (email !== '' && !this.email.invalid){
          this.email.markAsPending();
        }else {
          this.email.setErrors({'invalid': true});
        }
      })
    ).subscribe(
      email => {
        this.accountService.checkMail(email).subscribe(
          res => {
            if (res){
              this.email.markAsPending({onlySelf: false});
              this.email.setErrors(null);
            }else {
              this.email.markAsPending({onlySelf: false});
              this.email.setErrors({notUnique: true});
            }
          },error => {
            console.log(error);
            this.email.markAsPending({onlySelf: false});
            this.email.setErrors(null);
          }
        );
      }
    );
  }

  checkUsernameAvailability(){
    this.username.valueChanges.pipe(
      debounceTime(500),
      tap((username) => {
        if (username !== '' && !this.username.invalid){
          this.username.markAsPending();
        }else {
          this.username.setErrors({'invalid': true});
        }
      })
    ).subscribe(
      username => {
        this.accountService.checkUsername(username).subscribe(
          res => {
            if (res){
              this.username.markAsPending({onlySelf: false});
              this.username.setErrors(null);
            }else {
              this.username.markAsPending({onlySelf: false});
              this.username.setErrors({notUnique: true});
            }
          },error => {
            console.log(error);
            this.username.markAsPending({onlySelf: false});
            this.username.setErrors(null);
          }
        );
      }
    );
  }

  checkPasswordConfirmation(c: AbstractControl): { invalid: boolean } {
    if (c.get('password').value !== c.get('confirmation').value) {
      c.get('confirmation').setErrors({noMatch: true});
      return {invalid: true};
    }
  }

}
