import {Component, ElementRef, OnDestroy, OnInit} from '@angular/core';
import {UserAccountModel} from '../../model/UserAccount.model';
import {Subscription} from 'rxjs';
import {AccountService} from '../../service/account.service';
import {Router} from '@angular/router';
import {TutorialService} from '../../service/tutorial.service';
import {TutorialModel} from '../../model/Tutorial.model';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css']
})
export class UserDashboardComponent implements OnInit, OnDestroy {

  userAccount: UserAccountModel;
  userAccountSubscription: Subscription;
  userTutorials : TutorialModel[];
  userTutorialsSubscription: Subscription;
  modifyTutorial: boolean;


  constructor(private accountService: AccountService,
              private tutorialService: TutorialService,
              private elementRef: ElementRef,
              private router: Router) { }

  ngOnInit(): void {
    this.accountService.getLoggedUserAccount();
    this.accountService.emitUserAccount();
    this.userAccountSubscription = this.accountService.accountSubject.subscribe(
      (account: UserAccountModel) => {
        this.userAccount = account;
        this.tutorialService.getWriterTutorials(account.externalId);
      }
    );
    this.tutorialService.emitTutorials();
    this.userTutorialsSubscription = this.tutorialService.tutorialsSubject.subscribe(
      (tutorials: TutorialModel[]) => {
        this.userTutorials = tutorials;
      }
    );
    this.modifyTutorial = false;
  }

  ngOnDestroy(){
    this.userAccountSubscription.unsubscribe();
    this.userTutorialsSubscription.unsubscribe();
  }

  onTutorial(externalId: string) {
    if (!this.modifyTutorial)
      this.router.navigate(['tutorial/view/' + externalId]);
    else
      this.router.navigate(['tutorial/redaction/' + externalId]);
  }

  onNewTutorial(){
    this.router.navigate(['tutorial/create']);
  }

  onEditTutorial(){
    // const tag = this.elementRef.nativeElement.querySelector(".edit-icon");
    // if (!tag.classList.contains('collapse')){
    //   tag.classList.add('collapse');
    //   this.modifyTutorial = true;
    // }else {
    //   tag.classList.remove('collapse');
    //   this.modifyTutorial = false;
    // }
    this.modifyTutorial = this.modifyTutorial != true;
  }

}
