import { Component, OnInit } from '@angular/core';
import { version } from '../../../package.json';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  public version: string = version;
  public success = '';

  constructor() {
  }

  ngOnInit(): void {
    this.success = window.history.state.success;
  }

  closeAlert(){
    this.success = '';
  }

}
