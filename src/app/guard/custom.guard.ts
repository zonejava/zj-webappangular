import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import { OAuthService } from 'angular-oauth2-oidc';
import { Injectable } from '@angular/core';
import {TokenService} from '../service/token.service';

@Injectable({
  providedIn: 'root',
})
export class CustomGuard implements CanActivate {

  constructor(private oauthService: OAuthService,
              private tokenService: TokenService) {}

  canActivate(route: ActivatedRouteSnapshot, routerStateSnapshot: RouterStateSnapshot): boolean {

    if ( !this.tokenService.isLogged ) {
      this.oauthService.initLoginFlow(routerStateSnapshot.url);
    }

    return true;

  }
}
