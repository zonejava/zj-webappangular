import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CommentaryModel} from '../../model/Commentary.model';

@Component({
  selector: 'app-commentary-item',
  templateUrl: './commentary-item.component.html',
  styleUrls: ['./commentary-item.component.css']
})
export class CommentaryItemComponent implements OnInit {

  @Input() commentary: CommentaryModel;
  @Input() index: number;

  @Output() updateEvent: EventEmitter<CommentaryModel> = new EventEmitter();
  @Output() deleteEvent: EventEmitter<string> = new EventEmitter();

  @Input() loggedUserId: string;
  @Input() isLoggedUserAdmin: boolean;

  constructor() { }

  ngOnInit(): void {
  }

  onUpdateCommentary(commentary: CommentaryModel){
    this.updateEvent.emit(commentary);
  }

  onDeleteCommentary(commentaryId: string){
    this.deleteEvent.emit(commentaryId);
  }

}
