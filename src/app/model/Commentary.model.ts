import {UserAccountModel} from './UserAccount.model';

export class CommentaryModel {

  externalId: string;
  commentedId: string;
  content: string;
  writer: UserAccountModel;
  creationCommentaryDate: Date;
  updateCommentaryDate: Date;

  constructor(commentedId:string, content: string) {
    this.commentedId = commentedId;
    this.content = content;
  }

}
