export class VideoModel {

  externalId: string;
  source: string;
  link: string;
  addRepositoryDate: Date;

  constructor(source: string, link: string) {
    this.source = source;
    this.link = link;
  }

}
