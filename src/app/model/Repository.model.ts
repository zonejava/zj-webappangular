export class RepositoryModel {

  externalId: string;
  source: string;
  link: string;
  addVideoDate: Date;

  constructor(source: string, link: string) {
    this.source = source;
    this.link = link;
  }

}
