import {UserAccountModel} from './UserAccount.model';
import {ChapterModel} from './Chapter.model';
import {RepositoryModel} from './Repository.model';

export class TutorialModel {

  externalId: string;
  title: string;
  description: string;
  writer: UserAccountModel;
  themes: any[];
  chapters: ChapterModel[];
  rate: any[];
  remoteRepository: RepositoryModel;
  accessibility: string;
  status: string;
  publicationDate: Date;
  creationTutorialDate: Date;
  updateTutorialDate: Date;

  constructor(title: string, description: string) {
    this.title = title;
    this.description = description;
  }
}
