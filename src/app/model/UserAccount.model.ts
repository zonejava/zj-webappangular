export class UserAccountModel {

  externalId: string;
  password: string;
  biography: string
  registrationAccountDate: Date;

  constructor(public firstName: string,
              public lastName: string,
              public username: string,
              public email: string,
              password:string) {
    this.password = password;
  }

}
