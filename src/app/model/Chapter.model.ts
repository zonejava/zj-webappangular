import {VideoModel} from './Video.model';

export class ChapterModel {

  externalId: string;
  title: string;
  content: string;
  video: VideoModel;
  creationChapterDate: Date;
  updateChapterDate: Date;

  constructor() {

  }
}
