import { Injectable } from '@angular/core';
import {TutorialModel} from '../model/Tutorial.model';
import {Observable, Subject} from 'rxjs';
import {environment} from '../../environments/environment';
import {HttpClient, HttpParams} from '@angular/common/http';
import {ChapterModel} from '../model/Chapter.model';
import {RepositoryModel} from '../model/Repository.model';

@Injectable({
  providedIn: 'root'
})
export class TutorialService {

  tutorial: TutorialModel;
  tutorialSubject = new Subject<TutorialModel>();

  tutorials: TutorialModel[];
  tutorialsSubject = new Subject<TutorialModel[]>();
  currentPage: number;
  currentPageSubject = new Subject<number>();
  pages: number[];
  pageSubject = new Subject<number[]>();

  idCountMap: Map<string, number>;
  idCountMapSubject = new Subject<Map<string, number>>();

  noResult:boolean = false;
  noResultSubject = new Subject<boolean>();
  errorConnection:boolean = false;
  errorConnectionSubject = new Subject<boolean>();

  paramsId: string;

  constructor(private httpClient: HttpClient) { }

  emitTutorial() {
    this.tutorialSubject.next(this.tutorial);
  }

  emitTutorials(){
    this.tutorialsSubject.next(this.tutorials);
    this.pageSubject.next(this.pages);
    this.currentPageSubject.next(this.currentPage);
    this.noResultSubject.next(this.noResult);
    this.errorConnectionSubject.next(this.errorConnection);
  }

  emitCommentariesCount(){
    this.idCountMapSubject.next(this.idCountMap);
  }

  persistTutorialData(tutorial: TutorialModel) :Observable<any>{
    return this.httpClient.put<TutorialModel>(environment.endpointUrl.tutorial.saveTutorial,tutorial);
  }

  getTutorial(id: string){
    this.httpClient.get<TutorialModel>(environment.endpointUrl.tutorial.getTutorial + id).subscribe(
      (data) => {
        this.tutorial = data;
        this.emitTutorial();
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getPublishedTutorials(page: number, keyword: string){
    this.httpClient.get(environment.endpointUrl.tutorial.getPublishedTutorials + page + (keyword ? '?search=' + keyword : '') ).subscribe(
      (data) => {
        this.tutorials = data['content'];
        if (this.tutorials === null || this.tutorials.length === 0)
          this.noResult = true;
        this.errorConnection = false;
        this.getTutorialsIdAsParams();
        // this.getCommentariesCount(); TODO get commentaries count to display in cards
        this.pages = new Array(data['totalPages']);
        this.currentPage = data['number'];
        this.emitTutorials();
        },
      (err) => {
        this.errorConnection = true;
        this.noResult = false;
        this.emitTutorials();
        let message = "Error : can't connect to server";
        console.log(err.error.message?err.error.message:message);
      }
    );
  }

  getCommentariesCount() { // TODO
    this.httpClient.get(environment.endpointUrl.commentary.getCommentariesCount + '?' + this.paramsId).subscribe(
      (count: Map<string,number>) => {
        this.idCountMap = count;
        this.emitCommentariesCount();
      },error => {
        console.log(error.error.message);
      }
    );
  }

  getWriterTutorials(id: string){
    this.httpClient.get<TutorialModel[]>(environment.endpointUrl.tutorial.getUserTutorials + id).subscribe(
      (tutorial) => {
        this.tutorials = tutorial;
        this.emitTutorials();
      },
      (err) => {
        console.log(err);
      }
    );
  }

  saveTutorial(tutorial: TutorialModel) {
    this.httpClient.put<TutorialModel>(environment.endpointUrl.tutorial.updateTutorial,tutorial).subscribe(
      (tutorialUpdate) => {
        this.tutorial = tutorialUpdate;
        this.emitTutorial();
      },
      (err) => {
        console.log(err.error.message);
      }
    );
  }

  deleteTutorial(id: string) {
    this.httpClient.delete(environment.endpointUrl.tutorial.deleteTutorial + id).subscribe(
      () => {
        console.log('tutorial deleted !');
      },
      (err) => {
        console.log(err.error.message);
      }
    );
  }

  addChapterToTutorial(chapter: ChapterModel, tutoriaExternallId: string){
    this.httpClient.put<ChapterModel>(environment.endpointUrl.chapter.saveChapter + tutoriaExternallId,chapter).subscribe(
      () => {
        this.getTutorial(tutoriaExternallId);
        this.emitTutorial();
      },
      (err) => {
        console.log(err.error.message);
      }
    );
  }

  removeChapterFromTutorial(chapterExternalId: string){
    this.httpClient.delete(environment.endpointUrl.chapter.removeChapter + chapterExternalId).subscribe(
      () => {
        this.getTutorial(this.tutorial.externalId);
        this.emitTutorial();
      },(err) => {
        console.log(err.error.message);
      }
    );
  }

  addRepository(repository: RepositoryModel, tutorialExternalId: string) {
    this.httpClient.put<RepositoryModel>(environment.endpointUrl.tutorial.addRepository + tutorialExternalId,repository).subscribe(
      () => {
        this.getTutorial(tutorialExternalId);
        this.emitTutorial();
      },
      (err) => {
        console.log(err);
      }
    );
  }

  deleteRepository(repositoryExternalId: string, tutorialExternalId:string){
    this.httpClient.delete(environment.endpointUrl.tutorial.removeRepository + repositoryExternalId).subscribe(
      () => {
        // this.getTutorial(tutorialExternalId);
        this.tutorial.remoteRepository = null;
        this.emitTutorial();
      },error => {
        console.log(error);
      }
    );
  }

  publish(tutorialExternalId: string){
    this.httpClient.get<TutorialModel[]>(environment.endpointUrl.tutorial.publish + tutorialExternalId).subscribe(
      (tutorial) => {
        this.tutorials = tutorial;
        this.emitTutorials();
      },
      (err) => {
        console.log(err);
      }
    );
  }

  unPublish(tutorialExternalId: string){
    this.httpClient.get<TutorialModel[]>(environment.endpointUrl.tutorial.unpublish + tutorialExternalId).subscribe(
      (tutorial) => {
        this.tutorials = tutorial;
        this.emitTutorials();
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getTutorialsIdAsParams(){
    const params = [];
    for (let tutorial of this.tutorials){
      params.push(tutorial.externalId);
      params.push('=0');
      params.push('&');
    }
    params.splice(params.length - 1,params.length); // remove last '&'
    this.paramsId = params.join('');
  }

}
