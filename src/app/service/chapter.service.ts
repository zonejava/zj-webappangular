import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {TutorialService} from './tutorial.service';
import {ChapterModel} from '../model/Chapter.model';
import {environment} from '../../environments/environment';
import {VideoModel} from '../model/Video.model';

@Injectable({
  providedIn: 'root'
})
export class ChapterService {

  chapter: ChapterModel;
  // chapterSubject = new Subject<ChapterModel>();

  constructor(private httpClient: HttpClient,
              private tutorialService: TutorialService) {}

  // emitChapter(){ TODO used for tutorial display page
  //   this.chapterSubject.next(this.chapter-redaction);
  // }
  //
  // getChapter(id: string){
  //   this.httpClient.get<ChapterModel>(environment.endpointUrl.chapter-redaction.getChapter + id).subscribe(
  //     (chapter-redaction) => {
  //       this.chapter-redaction = chapter-redaction;
  //       this.emitChapter();
  //     },
  //     (err) => {
  //       console.log(err);
  //     }
  //   );
  // }

  saveChapter(chapter: ChapterModel, tutorialExternalId: string) {
    this.httpClient.put<ChapterModel>(environment.endpointUrl.chapter.updateChapter,chapter).subscribe(
      (chapterUpdate) => {
        this.chapter = chapterUpdate;
        this.tutorialService.getTutorial(tutorialExternalId);
        this.tutorialService.emitTutorial();
      },
      (err) => {
        console.log(err);
      }
    );
  }

  addVideo(video: VideoModel, chapterExternalId: string, tutorialExternalId: string) {
    this.httpClient.put<VideoModel>(environment.endpointUrl.chapter.addVideo + chapterExternalId,video).subscribe(
      () => {
        this.tutorialService.getTutorial(tutorialExternalId);
        this.tutorialService.emitTutorial();
      },
      (err) => {
        console.log(err);
      }
    );
  }

  deleteVideo(videoExternalId: string, tutorialExternalId:string){
    this.httpClient.delete(environment.endpointUrl.chapter.removeVideo + videoExternalId).subscribe(
      () => {
        this.tutorialService.getTutorial(tutorialExternalId);
        this.tutorialService.emitTutorial();
      },error => {
        console.log(error);
      }
    );
  }
}
