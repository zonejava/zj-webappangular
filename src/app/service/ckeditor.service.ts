import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CkeditorService {

  constructor() { }

  getConfigOfCKEditor(): any {
    const toolbarGroups = [
      '/',
      { name: 'document', groups: ['mode', 'doctools', 'document'] },
      { name: 'editing', groups: ['find', 'selection', 'spellchecker', 'editing'] },
      { name: 'forms', groups: ['forms'] },
      '/',
      { name: 'clipboard', groups: ['clipboard', 'undo'] },
      { name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi', 'paragraph'] },
      { name: 'basicstyles', groups: ['basicstyles', 'cleanup'] },
      { name: 'links', groups: ['links'] },
      { name: 'styles', groups: ['styles'] },
      { name: 'colors', groups: ['colors'] },
      { name: 'tools', groups: ['tools'] },
      { name: 'others', groups: ['others'] },
      { name: 'about', groups: ['about'] },
      { name: 'insert', groups: ['codesnippet'] }
    ];
    const removeButtons: string = 'Source,Templates,Save,NewPage,Print,Replace,Scayt,SelectAll,Form,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Blockquote,CreateDiv,Language,Image,Flash,Table,HorizontalRule,Smiley,SpecialChar,PageBreak,Iframe,ShowBlocks,About,Checkbox,Find,Preview,Styles,Format,Anchor';

    return {
      height: 400,
      toolbarGroups: toolbarGroups,
      removeButtons: removeButtons,
      extraPlugins: 'codesnippet',
      codeSnippet_theme: 'github',
      codeSnippet_languages: { javascript: 'JavaScript', php: 'PHP', java: 'Java', html: 'Html' }
    };
  }
}
