import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CommentaryModel} from '../model/Commentary.model';
import {Subject} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CommentaryService {

  commentaries: CommentaryModel[];
  commentariesSubject = new Subject<CommentaryModel[]>();

  constructor(private httpClient: HttpClient) { }

  emitCommentaries(){
    this.commentariesSubject.next(this.commentaries);
  }

  saveCommentary(commentaryToSave: CommentaryModel){
    this.httpClient.put<CommentaryModel[]>(environment.endpointUrl.commentary.saveCommentary, commentaryToSave).subscribe(
      (commentaries) => {
        this.commentaries = commentaries;
        this.emitCommentaries();
      },
      (err) => {
        console.log(err.error.message);
      }
    );
  }

  updateCommentary(commentaryToUpdate: CommentaryModel) {
    this.httpClient.put<CommentaryModel[]>(environment.endpointUrl.commentary.updateCommentary, commentaryToUpdate).subscribe(
      (commentaries) => {
        this.commentaries = commentaries;
        this.emitCommentaries();
      },
      (err) => {
        console.log(err.error.message);
      }
    );
  }

  deleteCommentary(id: string, tutorialId: string){
    this.httpClient.delete(environment.endpointUrl.commentary.deleteCommentary + id).subscribe(
      () => {
        this.getCommentaries(tutorialId);
      },
      (err) => {
        console.log(err.error.message);
      }
    );
  }

  getCommentaries(commentedId: string){
    this.httpClient.get<CommentaryModel[]>(environment.endpointUrl.commentary.getCommentaries + commentedId).subscribe(
      (data) => {
        this.commentaries = data;
        this.emitCommentaries();
      },
      (err) => {
        console.log(err.error.message);
      }
    );
  }

}
