import { Injectable } from '@angular/core';
import {UserAccountModel} from "../model/UserAccount.model";
import {environment} from "../../environments/environment";
import {HttpClient, HttpParams} from "@angular/common/http";
import {Observable, Subject} from "rxjs";
import {OAuthService} from 'angular-oauth2-oidc';
import {TokenService} from './token.service';

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  loggedUserId: string = '';

  account: UserAccountModel;
  accountSubject = new Subject<UserAccountModel>();

  constructor(private httpClient: HttpClient,
              private oAuthService: OAuthService,
              private tokenService: TokenService) { }

  emitUserAccount() {
    this.accountSubject.next(this.account);
  }

  createNewUser(userAccount: UserAccountModel) :Observable<any>{
    return this.httpClient.put(environment.endpointUrl.account.createUser,userAccount);
  }

  checkMail(mail: string){
    const opts = { params: new HttpParams({fromString: "mail=" + mail}) };
    return this.httpClient.get(environment.endpointUrl.account.checkMail, opts);
  }

  checkUsername(username: string){
    const opts = { params: new HttpParams({fromString: "username=" + username}) };
    return this.httpClient.get(environment.endpointUrl.account.checkUsername, opts);
  }

  getLoggedUserId() {
    this.loggedUserId = this.tokenService.payload.sub;
  }

  getLoggedUserAccount(){
    this.getLoggedUserId();
    this.httpClient.get<UserAccountModel>(environment.endpointUrl.account.getUser + this.loggedUserId).subscribe(
      (user) => {
        this.account = user;
        this.emitUserAccount();
      },
      (err) => {
        console.log(err.error.message);
      }
    );
  }

}
