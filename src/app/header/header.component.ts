import { Component, OnInit } from '@angular/core';
import {OAuthService} from 'angular-oauth2-oidc';
import {NgForm} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  isLogged: boolean;
  content: string;

  constructor(private oAuthService: OAuthService,
              private router: Router) { }

  ngOnInit(): void {
    this.isLogged = this.oAuthService.hasValidIdToken() && this.oAuthService.hasValidAccessToken();
  }

  onSubmit(e: NgForm) {
    this.content = e.value['content'];
    console.log('search content : ' + this.content);
    this.router.navigate(['/tutorial/list',{search:this.content}]);
  }

  onSignIn() {
    this.oAuthService.initLoginFlow('/account/dashboard');
  }

  onSignOut() {
    this.oAuthService.logOut();
  }

  resetSearch(){
    this.content = '';
  }
}

