import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, LOCALE_ID, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { HeaderComponent } from './header/header.component';
import { AccountFormComponent } from './account/account-form/account-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AccountService} from "./service/account.service";
import { WelcomeComponent } from './welcome/welcome.component';
import {OAuthModule} from 'angular-oauth2-oidc';
import {OauthInterceptor} from './interceptor/oauth.interceptor';
import {AuthInitializer} from './auth-initializer';
import {baseUrl} from '../environments/environment.prod';
import { UserDashboardComponent } from './account/user-dashboard/user-dashboard.component';
import {IconsModule, NavbarModule, WavesModule} from 'angular-bootstrap-md';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { TutorialFormComponent } from './tutorial/tutorial-form/tutorial-form.component';
import {MatInputModule} from '@angular/material/input';
import { TutorialRedactionComponent } from './tutorial/tutorial-redaction/tutorial-redaction.component';
import { ChapterRedactionComponent } from './tutorial/chapter-redaction/chapter-redaction.component';
import { TutorialViewComponent } from './tutorial/tutorial-view/tutorial-view.component';
import {CKEditorModule} from 'ckeditor4-angular';
import { ChapterViewComponent } from './tutorial/chapter-view/chapter-view.component';
import { VideoComponent } from './tutorial/add/video/video.component';
import { RepositoryComponent } from './tutorial/add/repository/repository.component';
import { ChangeStatusComponent } from './tutorial/change-status/change-status.component';
import { registerLocaleData } from '@angular/common';
import localeFr from '@angular/common/locales/fr';
import { TutorialListComponent } from './tutorial/tutorial-list/tutorial-list.component';
import { TutorialItemComponent } from './tutorial/tutorial-item/tutorial-item.component';
import {SortingByDatePipe} from './pipe/sorting-by-date.pipe';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { CommentaryListComponent } from './commentary/commentary-list/commentary-list.component';
import { CommentaryItemComponent } from './commentary/commentary-item/commentary-item.component';

// the second parameter 'fr' is optional
registerLocaleData(localeFr, 'fr');

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AccountFormComponent,
    WelcomeComponent,
    UserDashboardComponent,
    TutorialFormComponent,
    TutorialRedactionComponent,
    ChapterRedactionComponent,
    TutorialViewComponent,
    ChapterViewComponent,
    VideoComponent,
    RepositoryComponent,
    ChangeStatusComponent,
    TutorialListComponent,
    TutorialItemComponent,
    SortingByDatePipe,
    CommentaryListComponent,
    CommentaryItemComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        HttpClientModule,
        MDBBootstrapModule.forRoot(),
        OAuthModule.forRoot({
            resourceServer: {
                allowedUrls: ['http://localhost:9000', 'https://gateway.arnaudlaval-developer.com'],
                sendAccessToken: true
            }
        }),
        NavbarModule,
        WavesModule,
        IconsModule,
        MatInputModule,
        CKEditorModule,
        MatSnackBarModule,
    ],
  providers: [
    AccountService,
    {
      provide: 'GATEWAY_URL',
      useValue: baseUrl.gateway
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: OauthInterceptor,
      multi: true
    },
    {
      provide: APP_INITIALIZER,
      useFactory(authInitializer: AuthInitializer) {
        return () => authInitializer.init();
      },
      deps: [AuthInitializer],
      multi: true,
    },
    {provide: LOCALE_ID, useValue: 'fr' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
