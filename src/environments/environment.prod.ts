export const baseUrl = {
  gateway : window["env"]["gatewayUrl"] || "https://gateway.arnaudlaval-developer.com"
};

export const serviceUrls = {
  account: {
    baseUrl : baseUrl.gateway + '/ACCOUNT'
  },
  tutorial: {
    baseUrl : baseUrl.gateway + '/TUTORIAL'
  },
  commentary: {
    baseUrl : baseUrl.gateway + '/COMMENTARY'
  }
};

export const environment = {
  production: true,
  keycloak: {
    // Url of the Identity Provider
    issuer: 'https://www.auth.arnaudlaval-developer.com/auth/realms/zone-java',

    redirectUri: window.location.origin,

    clientId: 'zone-java-app',
    dummyClientSecret: '2d60113c-931b-41f0-a992-ec1b479c8742',
    responseType: 'code',
    scope: 'openid profile email',
    requireHttps: true,
    disableAtHashCheck: true,
    showDebugInformation: true
  },
  endpointUrl: {
    account: {
      createUser: serviceUrls.account.baseUrl + '/account/save',
      deleteUser: serviceUrls.account.baseUrl + '/account/remove/',
      getUser: serviceUrls.account.baseUrl + '/account/',
      checkMail: serviceUrls.account.baseUrl + '/account/check/mail',
      checkUsername: serviceUrls.account.baseUrl + '/account/check/username'
    },
    tutorial: {
      saveTutorial: serviceUrls.tutorial.baseUrl + '/tutorial/save',
      deleteTutorial: serviceUrls.tutorial.baseUrl + '/tutorial/delete/',
      getTutorial: serviceUrls.tutorial.baseUrl + '/tutorial/',
      updateTutorial: serviceUrls.tutorial.baseUrl + '/tutorial/update',
      getPublishedTutorials: serviceUrls.tutorial.baseUrl + '/tutorials/published/',
      getUserTutorials: serviceUrls.tutorial.baseUrl + '/user/tutorial/',
      addRepository: serviceUrls.tutorial.baseUrl + '/tutorial/repo/add/',
      removeRepository: serviceUrls.tutorial.baseUrl + '/tutorial/repo/remove/',
      publish: serviceUrls.tutorial.baseUrl + '/tutorial/publish/',
      unpublish: serviceUrls.tutorial.baseUrl + '/tutorial/unpublish/'
    },
    chapter: {
      saveChapter: serviceUrls.tutorial.baseUrl + '/chapter/add/',
      removeChapter: serviceUrls.tutorial.baseUrl + '/chapter/remove/',
      updateChapter: serviceUrls.tutorial.baseUrl + '/chapter/update',
      addVideo: serviceUrls.tutorial.baseUrl + '/chapter/video/add/',
      removeVideo: serviceUrls.tutorial.baseUrl + '/chapter/video/remove/'
    },
    commentary: {
      saveCommentary: serviceUrls.commentary.baseUrl + '/commentary/save',
      updateCommentary: serviceUrls.commentary.baseUrl + '/commentary/update',
      deleteCommentary: serviceUrls.commentary.baseUrl + '/commentary/delete/',
      getCommentaries: serviceUrls.commentary.baseUrl + '/commentaries/',
      getCommentariesCount: serviceUrls.commentary.baseUrl + '/commentaries/count'
    }
  }
};


